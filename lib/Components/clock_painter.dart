import 'dart:math';

import 'package:flutter/material.dart';

import '../Screens/clock_screen.dart';

class Painter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    //calculating the center of the circle
    var center = Offset(size.width / 2, size.height / 2);

    //defining the paint used to draw the outter path
    Paint clockPainer = Paint()
      ..color = const Color(0xFF1f2535)
      ..strokeWidth = 38
      ..strokeJoin = StrokeJoin.round
      ..style = PaintingStyle.stroke;

    //radius to draw the path
    var radius = min(size.width / 2, size.height / 2);

    //drawing the circular path
    canvas.drawCircle(center, radius, clockPainer);

    //paint used to draw the line represent the time left til the alarm
    Paint clockPainer2 = Paint()
      ..color =
          (TimeOfDay.now().period.name == 'am' ? Colors.amber : Colors.blue)
      ..strokeWidth = 38
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    //drawing the line based on the animation value
    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      2 * pi * (TimeOfDay.now().hour / 24),
      -2 * pi * (animation.value / 24),
      false,
      clockPainer2,
    );

    //start radius for the dashes
    var dashStartingPoint = radius - 30;
    //the end of the big dashes
    var bigDashEnd = radius - 45;
    //the end of the small dashes
    var smallDashEnd = radius - 40;

    //paint for the big dashes
    Paint bigDashPainter = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 4;

    //paint for the small dashes
    Paint smallDashPainter = Paint()
      ..color = Colors.grey
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 2;

    //painting the small dshes
    for (int i = 0; i < 360; i += 15) {
      //calculating x and y ordinates of the starting and ending point for
      //every dash in every i+15 degrees angle

      var x1 = size.width / 2 + dashStartingPoint * cos(i * pi / 180);
      var y1 = size.width / 2 + dashStartingPoint * sin(i * pi / 180);

      var x2 = size.width / 2 + smallDashEnd * cos(i * pi / 180);
      var y2 = size.width / 2 + smallDashEnd * sin(i * pi / 180);

      canvas.drawLine(Offset(x1, y1), Offset(x2, y2), smallDashPainter);
    }

    //painting the big dshes
    for (int i = 0; i < 360; i += 30) {
      var x1 = size.width / 2 + dashStartingPoint * cos(i * pi / 180);
      var y1 = size.width / 2 + dashStartingPoint * sin(i * pi / 180);

      var x2 = size.width / 2 + bigDashEnd * cos(i * pi / 180);
      var y2 = size.width / 2 + bigDashEnd * sin(i * pi / 180);

      canvas.drawLine(Offset(x1, y1), Offset(x2, y2), bigDashPainter);
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
